export interface User {
    email: string;
    displayName: string,
    token: string
}

//Mayusculas y minisculas
export interface Address {
    FirstName: string;
    LastName: string;
    Street: string;
    City: string;
    State: string;
    ZipCode: string;
}