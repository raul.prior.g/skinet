using Core.Entities;
using Core.Entities.OrderAggregate;
using Core.Interfaces;
using Core.Specification;

namespace Infrastructure.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBasketRepository _basketRepo;

        public OrderService(
            IUnitOfWork unitOfWork,
            IBasketRepository basketRepository
        )
        {
            _unitOfWork = unitOfWork;
            _basketRepo = basketRepository;
        }


        public async Task<Order> CreateOrderAsync(string buyerEmail, int deliveryMethosId, string basketId, Address shippingAdress)
        {
            //get basket form the repo
            var basket = await _basketRepo.GetBasketAsync(basketId);
            
            //get items form the product repo
            var items = new List<OrderItem>();
            foreach (var item in basket.Items){
                var productItem = await _unitOfWork.Repository<Product>().GetByIdAsync(item.Id);
                var itemOrder = new ProductItemOrdered(productItem.Id, productItem.Name, productItem.PictureUrl);
                var orderItem = new OrderItem(itemOrder, productItem.Price, item.Quantity);
                items.Add(orderItem);
            }

            //get delevery method form repo
            var deliveryMethos = await _unitOfWork.Repository<DeliveryMethod>().GetByIdAsync(deliveryMethosId);

            //calc subtotal 
            var subtotal = items.Sum(item => item.Price * item.Quantity);

            //create order
            var order = new Order(items, buyerEmail, shippingAdress, deliveryMethos, subtotal);
            _unitOfWork.Repository<Order>().Add(order);

            //save to DB
            var result = await _unitOfWork.Complete();

            if(result <= 0) return null;

            //delete basket
            await _basketRepo.DeleteBasketAsync(basketId);

            //return order
            return order; 
        }

        public async Task<IReadOnlyList<DeliveryMethod>> GetDeliveryMethodsAsync()
        {
            return await _unitOfWork.Repository<DeliveryMethod>().ListAllAsync();
        }

        public async Task<Order> GetOrderByIdAsync(int id, string buyerEmail)
        {
            var spec = new OrdersWithItemsAndOrderingSpecification(id, buyerEmail);
            return await _unitOfWork.Repository<Order>().GetEntityWithSpec(spec);
        }

        public async Task<IReadOnlyList<Order>> GetOrdersForUserAsync(string buyerEmail)
        {
            var spec = new OrdersWithItemsAndOrderingSpecification(buyerEmail);
            return await _unitOfWork.Repository<Order>().ListAsync(spec);
        }
    }
}