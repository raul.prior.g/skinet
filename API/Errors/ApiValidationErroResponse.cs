namespace API.Errors
{
    public class ApiValidationErroResponse : ApiResponse
    {
        public IEnumerable<String> Errors { get; set; }
        public ApiValidationErroResponse() : base(400)
        {
        }
    }
}