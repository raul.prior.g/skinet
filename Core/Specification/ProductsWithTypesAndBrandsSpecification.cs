using Core.Entities;

namespace Core.Specification
{
    public class ProductsWithTypesAndBrandsSpecification : BaseSpecification<Product>
    {
        public ProductsWithTypesAndBrandsSpecification(ProductSpecParams producParams)
            : base(x => 
                (!producParams.BrandId.HasValue || x.ProductBrandId == producParams.BrandId) &&
                (!producParams.TypeId.HasValue || x.ProductTypeId == producParams.TypeId) &&
                (string.IsNullOrEmpty(producParams.Search) || x.Name.ToLower().Contains(producParams.Search))
            )
        {
           AddInclude(x => x.ProductType); 
           AddInclude(x => x.ProductBrand); 
           AddOrderBy(x => x.Name);
           ApplyPaging(producParams.PageSize * (producParams.PageIndex -1), producParams.PageSize);

           if(!string.IsNullOrEmpty(producParams.Sort)){
                switch(producParams.Sort){
                    case "priceAsc":
                        AddOrderBy(x => x.Price);
                        break;
                    case "priceDesc":
                        AddOrderDescBy(x => x.Price);
                        break;
                    default:
                        AddOrderBy(x => x.Name);
                        break;
                }
           }
        }

        public ProductsWithTypesAndBrandsSpecification(int id) 
            : base(x => x.Id == id)
        {
            AddInclude(x => x.ProductType); 
            AddInclude(x => x.ProductBrand); 
        }
    }
}