using Core.Entities;

namespace Core.Specification
{
    public class ProductWithFilterForCountSpec : BaseSpecification<Product>
    {
        public ProductWithFilterForCountSpec(ProductSpecParams producParams)
            : base(x => 
                (!producParams.BrandId.HasValue || x.ProductBrandId == producParams.BrandId) &&
                (!producParams.TypeId.HasValue || x.ProductTypeId == producParams.TypeId) &&
                (string.IsNullOrEmpty(producParams.Search) || x.Name.ToLower().Contains(producParams.Search))
            )
        {
        }
    }
}